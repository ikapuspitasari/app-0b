package sari.ika.app0b

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.View
import android.widget.Toast
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.btnCamera -> {
                requestPermissions()
            }
            R.id.btnSend -> {
                uploadFile()
            }
        }

    }

    val url = "http://192.168.43.224/kampus/upload.php"
    var imstr = ""
    var namafile = ""
    var fileUri = Uri.parse("")
    lateinit var mediaHelper : MediaHelper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        try {
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        mediaHelper = MediaHelper()
        btnCamera.setOnClickListener(this)
        btnSend.setOnClickListener(this)
    }

    fun uploadFile() {
        val request = object : StringRequest(Method.POST, url,
            Response.Listener { response ->
                val jsonObject = JSONObject(response);
                val kode = jsonObject.getString("kode")
                if(kode.equals("000")){
                    Toast.makeText(this, "Upload foto sukses", Toast.LENGTH_LONG).show()
                } else {
                    Toast.makeText(this, "Upload foto gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }) {
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                hm.put("imstr", imstr)
                hm.put("namafile", namafile)
                return hm
            }
        }
        val q = Volley.newRequestQueue(this)
        q.add(request)
    }

    fun requestPermissions() = runWithPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA){
        fileUri = mediaHelper.getOutputMediaFileUri()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
        startActivityForResult(intent, mediaHelper.getRcCamera())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity. RESULT_OK)
            if(requestCode == mediaHelper.getRcCamera()){
                imstr = mediaHelper.getBitmapToString(imv, fileUri)
                namafile = mediaHelper.getMyFileName()
            }
    }
}
